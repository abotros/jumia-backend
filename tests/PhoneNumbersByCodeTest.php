<?php

namespace Http;

class PhoneNumbersByCodeTest extends \TestCase
{
    public function testPhoneNumbersByCode(): void
    {
        $this->json('GET', '/phone-numbers/258')
            ->seeJsonEquals(["phoneNumbers" => [
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 847651504"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 846565883"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 849181828"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 84330678235"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 847602609"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => false,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 042423566"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 823747618"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 848826725"
                ]
            ], "size" => 8]);
        $this->response->assertStatus(200);
    }

    public function testCodeValidation(): void
    {
        $this->json('GET', '/phone-numbers/221')
            ->seeJsonEquals([
                "countryCode" => [
                    "The selected country code is invalid."
                ]
            ]);
        $this->response->assertStatus(422);
    }

    public function testStateValidation(): void
    {
        $this->json('GET', '/phone-numbers/212?state=adsf')
            ->seeJsonEquals([
                "state" => [
                    "The selected state is invalid."
                ]
            ]);
        $this->response->assertStatus(422);
    }

    public function testStateAndCodeValidation(): void
    {
        $this->json('GET', '/phone-numbers/213?state=adsf')
            ->seeJsonEquals([
                "countryCode" => [
                    "The selected country code is invalid."
                ],
                "state" => [
                    "The selected state is invalid."
                ]
            ]);
        $this->response->assertStatus(422);
    }
}
