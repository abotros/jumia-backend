<?php

namespace Http;

class MorrocoPhoneNumbersTest extends \TestCase
{
    public function testMoroccoGetPhoneNumbers(): void
    {
        $this->json('GET', '/phone-numbers/212')
            ->seeJsonEquals(
                [
                    "phoneNumbers" => [
                        [
                            "countryName" => "Morocco",
                            "state" => true,
                            "countryCode" => "+212",
                            "phoneNumber" => "(212) 6007989253"
                        ],
                        [
                            "countryName" => "Morocco",
                            "state" => true,
                            "countryCode" => "+212",
                            "phoneNumber" => "(212) 698054317"
                        ],
                        [
                            "countryName" => "Morocco",
                            "state" => true,
                            "countryCode" => "+212",
                            "phoneNumber" => "(212) 6546545369"
                        ],
                        [
                            "countryName" => "Morocco",
                            "state" => true,
                            "countryCode" => "+212",
                            "phoneNumber" => "(212) 6617344445"
                        ],
                        [
                            "countryName" => "Morocco",
                            "state" => true,
                            "countryCode" => "+212",
                            "phoneNumber" => "(212) 691933626"
                        ],
                        [
                            "countryName" => "Morocco",
                            "state" => true,
                            "countryCode" => "+212",
                            "phoneNumber" => "(212) 633963130"
                        ],
                        [
                            "countryName" => "Morocco",
                            "state" => true,
                            "countryCode" => "+212",
                            "phoneNumber" => "(212) 654642448"
                        ]
                    ],
                    "size" => 7
                ]
            );
        $this->response->assertStatus(200);
    }

    public function testMoroccoValidGetPhoneNumbers(): void
    {
        $this->json('GET', '/phone-numbers/212?state=valid')
            ->seeJsonEquals(["phoneNumbers" => [
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 6007989253"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 698054317"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 6546545369"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 6617344445"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 691933626"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 633963130"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 654642448"
                ]
            ], "size" => 7]);
        $this->response->assertStatus(200);

    }

    public function testMoroccoNotValidGetPhoneNumbers(): void
    {
        $this->json('GET', '/phone-numbers/212?state=notValid')
            ->seeJsonEquals(["phoneNumbers" => [], "size" => 0]);
        $this->response->assertStatus(200);
    }
}
