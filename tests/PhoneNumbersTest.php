<?php

class PhoneNumbersTest extends TestCase
{
    public function testGetPhoneNumbers(): void
    {
        $this->json('GET', '/phone-numbers')
            ->seeJsonEquals(["phoneNumbers" => [
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 6007989253"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 698054317"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 6546545369"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 6617344445"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 691933626"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 633963130"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 654642448"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 847651504"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 846565883"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 849181828"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 84330678235"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 847602609"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => false,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 042423566"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 823747618"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 848826725"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 775069443"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => false,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 7503O6263"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 704244430"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 7734127498"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 7771031454"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 3142345678"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 714660221"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 9773199405"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 914701723"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 911203317"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 9119454961"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 914148181"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 966002259"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 988200000"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 924418461"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 911168450"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 697151594"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 677046616"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => false,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 6A0311634"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 673122155"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 695539786"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 6780009592"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 6622284920"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 696443597"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 691816558"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 699209115"
                ]
            ], "size" => 41]);
        $this->response->assertJsonStructure([
            'phoneNumbers' => [
                [
                    'countryName',
                    'state',
                    'countryCode',
                    'phoneNumber'
                ],

            ], 'size']);
        $this->response->assertStatus(200);
    }

    public function testGetValidPhoneNumbers(): void
    {
        $this->json('GET', '/phone-numbers/?state=valid')
            ->seeJsonEquals(["phoneNumbers" => [
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 6007989253"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 698054317"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 6546545369"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 6617344445"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 691933626"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 633963130"
                ],
                [
                    "countryName" => "Morocco",
                    "state" => true,
                    "countryCode" => "+212",
                    "phoneNumber" => "(212) 654642448"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 847651504"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 846565883"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 849181828"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 84330678235"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 847602609"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 823747618"
                ],
                [
                    "countryName" => "Mozambique",
                    "state" => true,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 848826725"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 775069443"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 704244430"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 7734127498"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 7771031454"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 3142345678"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => true,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 714660221"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 9773199405"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 914701723"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 911203317"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 9119454961"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 914148181"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 966002259"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 988200000"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 924418461"
                ],
                [
                    "countryName" => "Ethiopia",
                    "state" => true,
                    "countryCode" => "+251",
                    "phoneNumber" => "(251) 911168450"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 697151594"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 677046616"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 673122155"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 695539786"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 6780009592"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 6622284920"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 696443597"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 691816558"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => true,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 699209115"
                ]
            ], "size" => 38]);
        $this->response->assertStatus(200);
    }

    public function testGetNotValidPhoneNumbers(): void
    {
        $this->json('GET', '/phone-numbers/?state=notValid')
            ->seeJsonEquals(["phoneNumbers" => [
                [
                    "countryName" => "Mozambique",
                    "state" => false,
                    "countryCode" => "+258",
                    "phoneNumber" => "(258) 042423566"
                ],
                [
                    "countryName" => "Uganda",
                    "state" => false,
                    "countryCode" => "+256",
                    "phoneNumber" => "(256) 7503O6263"
                ],
                [
                    "countryName" => "Cameroon",
                    "state" => false,
                    "countryCode" => "+237",
                    "phoneNumber" => "(237) 6A0311634"
                ]
            ], "size" => 3]);
        $this->response->assertStatus(200);
    }

    public function testStateValidation(): void
    {
        $this->json('GET', '/phone-numbers/?state=jkjbhg')
            ->seeJsonEquals([
                "state" => [
                    "The selected state is invalid."
                ]
            ]);
        $this->response->assertStatus(422);
    }
}
