<?php

namespace App\Http\Controllers;

use App\Providers\Services\PhoneNumbersService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller as BaseController;

class PhoneNumbersController extends BaseController
{
    private $phoneNumbersService;

    public function __construct(PhoneNumbersService $phoneNumbersService)
    {
        $this->phoneNumbersService = $phoneNumbersService;
    }

    /**
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function getPhoneNumbers(Request $request)
    {
        $this->validate($request, [
            'state' => ['string', 'in:valid,notValid'],
            'page' => ['string', 'numeric'],
            'limit' => ['string', 'numeric'],
        ]);
        $state = $request->get('state');
        $page = $request->get('page');
        $limit = $request->get('limit');
        return $this->phoneNumbersService->getPhoneNumbers($state, $page, $limit);
    }

    /**
     * @param Request $request
     * @param string $countryCode
     * @return array
     * @throws ValidationException
     */
    public function getPhoneNumbersByCountryCode(Request $request, string $countryCode): array
    {
        $request->merge(['countryCode' => $countryCode]);
        $this->validate($request, [
            'countryCode' => ['required', 'string', 'in:237,251,212,258,256'],
            'state' => ['string', 'in:valid,notValid'],
            'page' => ['string', 'numeric'],
            'limit' => ['string', 'numeric'],
        ]);
        $state = $request->get('state');
        $page = $request->get('page');
        $limit = $request->get('limit');
        return $this->phoneNumbersService->getPhoneNumbersByCountryCode($countryCode, $state, $page, $limit);
    }
}
