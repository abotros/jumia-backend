<?php

namespace App\Responses;

class PaginationResponse implements \JsonSerializable
{
    public $phoneNumbers;
    public $size;

    public function __construct(array $phoneNumbers, int $size)
    {
        $this->phoneNumbers = $phoneNumbers;
        $this->size = $size;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
