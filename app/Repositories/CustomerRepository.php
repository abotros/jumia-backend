<?php

namespace App\Repositories;

use App\Models\Customer;
use Illuminate\Support\Collection;

class CustomerRepository implements CustomerRepositoryInterface
{
    /**
     * @return array
     */
    public function getAll(): array
    {
        return Customer::all();
    }

    /**
     * @param string $id
     * @return array
     */
    public function getCustomers(string $id): array
    {
        return Customer::find($id)->toArray();
    }

    /**
     * @return Collection
     */
    public function getPhoneNumbers(): Collection
    {
        return Customer::select('phone')->get();
    }

    /**
     * @param string $prefix
     * @return Collection
     */
    public function getPhoneNumbersByPrefix(string $prefix): Collection
    {
        return Customer::select('phone')->where('phone', 'like', '(' . $prefix . '%')->get();
    }
}
