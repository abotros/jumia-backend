<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

interface CustomerRepositoryInterface
{
    public function getAll(): array;

    public function getCustomers(string $id): array;

    public function getPhoneNumbers(): Collection;

    public function getPhoneNumbersByPrefix(string $prefix): Collection;
}
