<?php

namespace App\Dtos;

class PhoneNumberDto implements \JsonSerializable
{
    private $countryName;
    private $state;
    private $countryCode;
    private $phoneNumber;

    public function __construct(string $countryName, bool $state, string $countryCode, string $phoneNumber)
    {
        $this->countryName = $countryName;
        $this->state = $state;
        $this->countryCode = $countryCode;
        $this->phoneNumber = $phoneNumber;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
