<?php

namespace App\Dtos;

class CountryDto
{
    private $countryName;
    private $countryCode;
    private $regex;
    private $prefix;

    public function __construct(string $countryName, string $countryCode, string $regex, string $prefix)
    {
        $this->countryName = $countryName;
        $this->countryCode = $countryCode;
        $this->regex = $regex;
        $this->prefix = $prefix;
    }

    /**
     * @return string
     */
    public function getCountryName(): string
    {
        return $this->countryName;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @return string
     */
    public function getRegex(): string
    {
        return $this->regex;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

}
