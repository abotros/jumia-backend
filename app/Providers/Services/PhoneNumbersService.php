<?php

namespace App\Providers\Services;

use App\Dtos\PhoneNumberDto;
use App\Enums\StateEnum;
use App\Helpers\CountryHelper;
use App\Repositories\CustomerRepositoryInterface;
use App\Responses\PaginationResponse;
use Illuminate\Support\Collection;

class PhoneNumbersService
{
    private $customerRepository;
    private $countryHelper;

    public function __construct(CustomerRepositoryInterface $customerRepository, CountryHelper $countryHelper)
    {
        $this->customerRepository = $customerRepository;
        $this->countryHelper = $countryHelper;
    }

    /**
     * @param string|null $state
     * @return array
     */
    public function getPhoneNumbers(?string $state, ?int $page, ?int $limit): array
    {
        $phoneNumbersCollection = $this->customerRepository->getPhoneNumbers();
        $collection = collect($this->getPhoneNumbersByState($phoneNumbersCollection, $state));
        $size = $collection->count();
        $response = new PaginationResponse($collection->forPage($page, $limit)->toArray(), $size);
        return json_decode(json_encode($response), true);
    }

    /**
     * @param string $countryCode
     * @param string|null $state
     * @return array
     */
    public function getPhoneNumbersByCountryCode(string $countryCode, ?string $state, ?int $page, ?int $limit): array
    {
        $phoneNumbersByCountry = $this->customerRepository->getPhoneNumbersByPrefix($countryCode);
        $collection = collect($this->getPhoneNumbersByState($phoneNumbersByCountry, $state));
        $size = $collection->count();
        $response = new PaginationResponse($collection->forPage($page, $limit)->toArray(), $size);
        return json_decode(json_encode($response), true);
    }

    /**
     * @param Collection $phoneNumbersCollection
     * @param string|null $state
     * @return array
     */
    private function getPhoneNumbersByState(Collection $phoneNumbersCollection, ?string $state): array
    {
        $phoneNumbers = [];
        foreach ($phoneNumbersCollection as $phoneNumber) {
            $prefix = $this->getCountryCode($phoneNumber['phone']);
            $country = $this->countryHelper->getCountry($prefix);
            $valid = $this->validatePhoneNumber($country->getRegex(), $phoneNumber);
            $phone = new PhoneNumberDto($country->getCountryName(), $valid, $country->getCountryCode(), $phoneNumber->phone);
            switch ($state) {
                case StateEnum::VALID:
                    if ($valid) {
                        $phoneNumbers[] = json_decode(json_encode($phone), true);
                    }
                    break;
                case StateEnum::NOT_VALID:
                    if (!$valid) {
                        $phoneNumbers[] = json_decode(json_encode($phone), true);
                    }
                    break;
                default:
                    $phoneNumbers[] = json_decode(json_encode($phone), true);
            }
        }
        return $phoneNumbers;
    }

    /**
     * @param string $phoneNumber
     * @return string
     */
    private function getCountryCode(string $phoneNumber): string
    {
        preg_match('#\((.*?)\)#', $phoneNumber, $match);
        return $match[1];
    }

    /**
     * @param string $regex
     * @param string $phoneNumber
     * @return bool
     */
    private function validatePhoneNumber(string $regex, string $phoneNumber): bool
    {
        return preg_match($regex, $phoneNumber);
    }
}
