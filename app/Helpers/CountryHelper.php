<?php

namespace App\Helpers;

use App\Dtos\CountryDto;

class CountryHelper
{
    private $countries;

    public function __construct()
    {
        $this->countries = [
            "237" => (new CountryDto("Cameroon", "+237", "/\(237\)\ ?[2368]\d{7,8}/", "(237)")),
            "251" => (new CountryDto("Ethiopia", "+251", "/\(251\)\ ?[1-59]\d{8}/", "(251)")),
            "212" => (new CountryDto("Morocco", "+212", "/\(212\)\ ?[5-9]\d{8}/", "(212)")),
            "258" => (new CountryDto("Mozambique", "+258", "/\(258\)\ ?[28]\d{7,8}/", "(258)")),
            "256" => (new CountryDto("Uganda", "+256", "/\(256\)\ ?\d{9}/", "(256)"))
        ];
    }

    /**
     * @return \string[][]
     */
    public function getCountries(): array
    {
        return $this->countries;
    }

    public function getCountry($countryIsoCode): CountryDto
    {
        return $this->countries[$countryIsoCode];
    }
}
