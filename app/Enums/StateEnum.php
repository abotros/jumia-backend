<?php

namespace App\Enums;

class StateEnum
{
    public const ALL = 'all';
    public const VALID = 'valid';
    public const NOT_VALID = 'notValid';
}
